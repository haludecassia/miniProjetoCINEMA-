﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for Mapped
/// </summary>
public class Mapped
{
    public static IDbConnection Conexao()
    {
        MySqlConnection objConexao = new MySqlConnection(ConfigurationManager.AppSettings["strConexao"]);
        objConexao.Open();
        return objConexao;
    }

    public static IDbCommand Comando(string query, IDbConnection objConexao)
    {
        IDbCommand comando = objConexao.CreateCommand();
        comando.CommandText = query;
        return comando;
    }

    public static IDbDataParameter Parametro(string nomeparametro, object valor)
    {
        return new MySqlParameter(nomeparametro, valor);
    }

    public static IDbDataAdapter Adapter(IDbCommand comando)
    {
        IDbDataAdapter adap = new MySqlDataAdapter();
        adap.SelectCommand = comando;
        return adap;
    }
}
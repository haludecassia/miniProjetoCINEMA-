﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for FilmeDB
/// </summary>
public class FilmeDB
{
    public static int Insert(Filme fil)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "insert into fil_filme values (0, ?nome, ?duracao, ?sinopse, ?lancamento, ?categoria, ?imagem);";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?nome", fil.fil_nome));
            objComando.Parameters.Add(Mapped.Parametro("?duracao", fil.fil_duracao));
            objComando.Parameters.Add(Mapped.Parametro("?sinopse", fil.fil_sinopse));
            objComando.Parameters.Add(Mapped.Parametro("?lancamento", fil.fil_lancamento));
            objComando.Parameters.Add(Mapped.Parametro("?categoria", fil.fil_categoria.cat_id));
            objComando.Parameters.Add(Mapped.Parametro("?imagem", fil.fil_imagem));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }


    public static int Atualizar(Filme fil)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "update fil_filme set fil_nome=?nome, fil_duracao=?duracao, fil_sinopse=?sinopse, fil_lancamento=?lancamento, fil_categoria=?categoria, fil_imagem=?imagem where fil_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?nome", fil.fil_nome));
            objComando.Parameters.Add(Mapped.Parametro("?duracao", fil.fil_duracao));
            objComando.Parameters.Add(Mapped.Parametro("?sinopse", fil.fil_sinopse));
            objComando.Parameters.Add(Mapped.Parametro("?lancamento", fil.fil_lancamento));
            objComando.Parameters.Add(Mapped.Parametro("?categoria", fil.fil_categoria.cat_id));
            objComando.Parameters.Add(Mapped.Parametro("?imagem", fil.fil_imagem));
            objComando.Parameters.Add(Mapped.Parametro("?id", fil.fil_id));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }


    public static Filme Select(int fil_id)
    {
        Filme fil = null;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            IDataReader objReader;
            objConexao = Mapped.Conexao();

            string sql = "select * from fil_filme where fil_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", fil_id));
            objReader = objComando.ExecuteReader();

            int categoria = 0;

            while (objReader.Read())
            {
                fil = new Filme();
                fil.fil_id = Convert.ToInt32(objReader["fil_id"]);
                fil.fil_nome = Convert.ToString(objReader["fil_nome"]);
                fil.fil_sinopse = Convert.ToString(objReader["fil_sinopse"]);
                fil.fil_lancamento = Convert.ToString(objReader["fil_lancamento"]);
                fil.fil_duracao = Convert.ToString(objReader["fil_duracao"]);
                categoria = Convert.ToInt32(objReader["fil_categoria"]);
                fil.fil_imagem = Convert.ToString(objReader["fil_imagem"]);

            }

            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
            fil.fil_categoria = CategoriaDB.Select(categoria);

            return fil;

        }

        catch (Exception e)
        {
            return fil = null;
        }
    }

    public static DataSet SelectAll()
    {
        DataSet ds = null;

        try
        {
            IDbConnection objConexao = Mapped.Conexao();
            IDbCommand objComando;
            IDataAdapter objAdapter;

            string sql = "select * from fil_filme;";

            objComando = Mapped.Comando(sql, objConexao);
            objAdapter = Mapped.Adapter(objComando);
            ds = new DataSet();
            objAdapter.Fill(ds);
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch (Exception e)
        {
            ds = null;
        }

        return ds;
    }

    public static int Delete(int fil_id)
    {
        int erro = 0;
        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "delete from fil_filme where fil_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", fil_id));
            objComando.ExecuteNonQuery();
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch
        {
            erro = -2;
        }

        return erro;
    }



}
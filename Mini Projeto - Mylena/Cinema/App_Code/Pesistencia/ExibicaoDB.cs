﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for ExibicaoDB
/// </summary>
public class ExibicaoDB
{
    public static int Insert(Exibicao exb)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "insert into exb_exibicao values (0, ?data, ?horario, ?filme, ?sala);";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?data", exb.exb_data));
            objComando.Parameters.Add(Mapped.Parametro("?horario", exb.exb_horario));
            objComando.Parameters.Add(Mapped.Parametro("?filme", exb.exb_filme.fil_id));
            objComando.Parameters.Add(Mapped.Parametro("?sala", exb.exb_sala.sal_id));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }


    public static int Atualizar(Exibicao exb)
    {
        int erro = 0;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "update exb_exibicao set exb_data=?data, exb_horario=?horario, exb_filme=?filme, exb_sala=?sala where exb_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?data", exb.exb_data));
            objComando.Parameters.Add(Mapped.Parametro("?horario", exb.exb_horario));
            objComando.Parameters.Add(Mapped.Parametro("?filme", exb.exb_filme.fil_id));
            objComando.Parameters.Add(Mapped.Parametro("?sala", exb.exb_sala.sal_id));
            objComando.Parameters.Add(Mapped.Parametro("?id", exb.exb_id));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            erro = -2;
        }

        return erro;
    }

    public static Exibicao Select(int exb_id)
    {
        Exibicao exb = null;

        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            IDataReader objReader;
            objConexao = Mapped.Conexao();

            string sql = "select * from exb_exibicao where exb_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", exb_id));
            objReader = objComando.ExecuteReader();

            int filmes = 0;
            int salas = 0;

            while (objReader.Read())
            {
                exb = new Exibicao();
                exb.exb_id = Convert.ToInt32(objReader["exb_id"]);
                exb.exb_data= Convert.ToString(objReader["exb_data"]);
                exb.exb_horario = Convert.ToString(objReader["exb_horario"]);
                filmes = Convert.ToInt32(objReader["exb_filme"]);
                salas = Convert.ToInt32(objReader["exb_sala"]);
            }

            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
            exb.exb_filme = FilmeDB.Select(filmes);
            exb.exb_sala = SalaDB.Select(salas);

            return exb;

        }

        catch (Exception e)
        {
            return exb = null;
        }
    }

    public static DataSet SelectAll()
    {
        DataSet ds = null;

        try
        {
            IDbConnection objConexao = Mapped.Conexao();
            IDbCommand objComando;
            IDataAdapter objAdapter;

            string sql = "select * from exb_exibicao;";

            objComando = Mapped.Comando(sql, objConexao);
            objAdapter = Mapped.Adapter(objComando);
            ds = new DataSet();
            objAdapter.Fill(ds);
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch (Exception e)
        {
            ds = null;
        }

        return ds;
    }

    public static int Delete(Exibicao exb)
    {
        int erro = 0;
        try
        {
            IDbConnection objConexao;
            IDbCommand objComando;
            objConexao = Mapped.Conexao();

            string sql = "delete from exb_exibicao where exb_id=?id;";

            objComando = Mapped.Comando(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parametro("?id", exb.exb_id));
            objComando.ExecuteNonQuery();
            objComando.Dispose();
            objConexao.Dispose();
            objConexao.Close();
        }
        catch
        {
            erro = -2;
        }

        return erro;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_CadastroSala : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Sala sal = new Sala();
        sal.sal_nome = txbNome.Text;
        sal.sal_capacidade = txbCapacidade.Text;
        sal.sal_tamanhoTela = Convert.ToString(ddlTamanho.SelectedItem);

        SalaDB.Insert(sal);
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txbNome.Text = "";
        txbCapacidade.Text = "";
        ddlTamanho.SelectedIndex = 0;
    }
}
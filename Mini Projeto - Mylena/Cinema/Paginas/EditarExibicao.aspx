﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditarExibicao.aspx.cs" Inherits="Paginas_EditarExibicao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><fieldset>
        
        <legend>Editar Exibição</legend>
        

    <table>

        <tr>
            <td>
                <asp:Label ID="lblCodigo" runat="server" Text="Codigo: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txbCodigo" runat="server" CssClass="tb" Enabled="false"></asp:TextBox>
            </td>

            <td>
                <asp:Label ID="lblFilme" runat="server" Text="Filme: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlFilme" runat="server" Width="170px" Height="30px"></asp:DropDownList>
            </td>

        </tr>

        <tr>
            <td>
                <asp:Label ID="lblData" runat="server" Text="Data: "></asp:Label>
           </td>
            <td>    
                <asp:TextBox ID="txbData" CssClass="tb" runat="server" placeholder="Digite a data filme:" ></asp:TextBox>
            </td>

            <td>
                <asp:Label ID="lblSala" runat="server" Text="Sala: "></asp:Label>
                </td>
           
            <td>
                <asp:DropDownList ID="ddlSala" runat="server" Width="170px" Height="30px"></asp:DropDownList>
            </td>

        </tr>
         <tr>
            <td>
                <asp:Label ID="lblHora" runat="server" Text="Hora: "></asp:Label>
           </td>
            <td>    
                <asp:DropDownList ID="ddlHora" runat="server" Width="170px" Height="30px">
                    <asp:ListItem Value="1">14:00</asp:ListItem>
                    <asp:ListItem Value="2">16:00</asp:ListItem>
                    <asp:ListItem Value="3">20:00</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>      

        <tr>
          
            <td colspan="2"><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td colspan="2"><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Excluir" OnClick="btnLimpar_Click"></asp:Button></td>
         
        </tr>
        </table>
        
    </fieldset>
    </center>
</asp:Content>


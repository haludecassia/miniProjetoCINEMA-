﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cinema.Funcoes;

public partial class Paginas_EditarExibicao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["exi"] != null && Request.QueryString["sal"] != null && Request.QueryString["fil"] != null)
            {
                if (Request.QueryString["exi"] != "" && Request.QueryString["sal"] != "" && Request.QueryString["fil"] != "")
                {
                    try
                    {
                        string exi1 = Request.QueryString["exi"].ToString().Replace(" ", "+");
                        int n1 = Convert.ToInt32(Funcoes.AESDecodifica(exi1));

                        string sal1 = Request.QueryString["sal"].ToString().Replace(" ", "+");
                        int n2 = Convert.ToInt32(Funcoes.AESDecodifica(sal1));

                        string fil1 = Request.QueryString["fil"].ToString().Replace(" ", "+");
                        int n3 = Convert.ToInt32(Funcoes.AESDecodifica(fil1));


                        Exibicao exi = ExibicaoDB.Select(n1);

                        txbCodigo.Text = Convert.ToString(exi.exb_id);
                        txbData.Text = exi.exb_data;

                        DataSet ds = SalaDB.SelectAll();
                        ddlSala.DataSource = ds;
                        ddlSala.DataTextField = "sal_nome";
                        ddlSala.DataValueField = "sal_id";
                        ddlSala.DataBind();
                        ddlSala.Items.Insert(0, "Selecionar...");
                        ddlSala.SelectedIndex = n2;

                        DataSet ds1 = FilmeDB.SelectAll();
                        ddlFilme.DataSource = ds1;
                        ddlFilme.DataTextField = "fil_nome";
                        ddlFilme.DataValueField = "fil_id";
                        ddlFilme.DataBind();
                        ddlFilme.Items.Insert(0, "Selecionar...");
                        ddlFilme.SelectedIndex = n3;
                    }
                    catch (Exception erro)
                    {
                        Response.Redirect("~/Paginas/ConsultaExibicao.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("~/Paginas/ConsultaExibicao.aspx");
            }
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Exibicao exb = new Exibicao();
        Filme fil = new Filme();
        Sala sal = new Sala();
        exb.exb_filme = fil;
        exb.exb_sala = sal;

        exb.exb_id = Convert.ToInt32(txbCodigo.Text);
        exb.exb_data = txbData.Text;
        exb.exb_horario = ddlHora.SelectedItem.Text;
        exb.exb_filme.fil_id = Convert.ToInt32(ddlFilme.SelectedValue);
        exb.exb_sala.sal_id= Convert.ToInt32(ddlSala.SelectedValue);

        switch (ExibicaoDB.Atualizar(exb))
        {
            case 0:
                Response.Redirect("~/Paginas/Default.aspx");
                break;
            
        }
        


    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {

    }
}
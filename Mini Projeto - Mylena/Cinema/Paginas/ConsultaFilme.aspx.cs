﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cinema.Funcoes;

public partial class Paginas_ConsultaFilme : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataSet ds = new DataSet();
            ds = FilmeDB.SelectAll();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string nome = Convert.ToString(dr["fil_nome"]).Replace(' ', '-');
                string imagem = Convert.ToString(dr["fil_imagem"]);
                Categoria cat = CategoriaDB.Select(Convert.ToInt32(dr["fil_categoria"]));
                lbl.Text += "<fieldset class='consulta'>" +
                                "<img src='../Imagens/" + nome + "/" + imagem + "' />" +
                                "<br/><center>Filme: " + dr["fil_nome"] + "<br />" +
                                "Duração: " + dr["fil_duracao"] + "<br />" +
                                "Sinopse: " + dr["fil_sinopse"] + "<br />" +
                                "Ano de Lançamento: " + dr["fil_lancamento"] + "<br />" +
                                "Categoria: " + cat.cat_descricao + "</center>" +
                                "<a href='../Paginas/EditarFilme.aspx?fil=" + Funcoes.AESCodifica(Convert.ToString(dr["fil_id"])) + "&cat=" + Funcoes.AESCodifica(Convert.ToString(dr["fil_categoria"])) + "'>" +
                                    "<br/><button type='button' class='Salvar'>" +
                                        "Editar" +
                                    "</button>" +
                                "</a>" +
                            "</fieldset>";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Cinema.Funcoes;
using System.Data;

public partial class Paginas_EditarCategoria : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["cat"] != null)
            {
                if (Request.QueryString["cat"] != "")
                {
                    try
                    {
                        string cat1 = Request.QueryString["cat"].ToString().Replace(" ", "+");
                        int n1 = Convert.ToInt32(Funcoes.AESDecodifica(cat1));

                        Categoria cat = CategoriaDB.Select(n1);

                        txbCodigo.Text = Convert.ToString(cat.cat_id);
                        txbNome.Text = cat.cat_descricao;
                    }
                    catch (Exception erro)
                    {
                        Response.Redirect("~/Paginas/ConsultaCategoria.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("~/Paginas/ConsultaCategoria.aspx");
            }
        }
    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Categoria cat = new Categoria();

        cat.cat_id = Convert.ToInt32(txbCodigo.Text);
        cat.cat_descricao = txbNome.Text;

        switch (CategoriaDB.Atualizar(cat))
        {
            case 0:
                Response.Redirect("~/Paginas/ConsultaCategoria.aspx");
                break;
            
        }
    }
    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        return;
    }
}
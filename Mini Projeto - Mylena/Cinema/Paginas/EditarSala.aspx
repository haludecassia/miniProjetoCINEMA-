﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditarSala.aspx.cs" Inherits="Paginas_EditarSala" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <center><fieldset>
        
        <legend>Editar Salas</legend>
        

    <table>

        <tr>
            <td>
                <asp:Label ID="lblCodigo" runat="server" Text="Codigo: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txbCodigo" runat="server" CssClass="tb" Enabled="false"></asp:TextBox>
            </td>

           <td>
                <asp:Label ID="lblTamanho" runat="server" Text="Tamanho da Tela: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlTamanho" runat="server" Width="170px" Height="30px">
                    <asp:ListItem Value="0" Text="Selecionar..."></asp:ListItem>
                    <asp:ListItem Value="1" Text="Extreme"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Normal"></asp:ListItem>
                </asp:DropDownList>
            </td>

        </tr>

        <tr>
            <td>
                <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbNome" CssClass="tb" runat="server" placeholder="Digite o nome da sala" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblCapacidade" runat="server" Text="Capacidade: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbCapacidade" CssClass="tb" runat="server" placeholder="Digite a capacidade da sala" ></asp:TextBox>

            </td>
            
        </tr>
        
        <tr>
            <td></td>
            <td><asp:Button ID="btnGravar" CssClass="Salvar" runat="server" Text="Gravar" OnClick="btnGravar_Click"></asp:Button></td>
            <td><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Excluir" OnClick="btnLimpar_Click"></asp:Button></td>
            <td></td>
        </tr>
        </table>
    </fieldset></center>


</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CadastroExibicao.aspx.cs" Inherits="Paginas_CadastroExibicao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><fieldset>
        
        <legend>Cadastro de Exibições</legend>
        

    <table>
        <tr>
            <td>
                <asp:Label ID="lblData" runat="server" Text="Data: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbData" CssClass="tb" runat="server" placeholder="Digite a data do filme" ></asp:TextBox>

            </td>

            <td>
                <asp:Label ID="lblHora" runat="server" Text="Horário: "></asp:Label>
            </td>
            <td>    
                <asp:DropDownList ID="ddlHora" runat="server" Width="170px" Height="30px">
                    <asp:ListItem Value="0" Text="Selecionar..."></asp:ListItem>
                    <asp:ListItem Value="1">16:00</asp:ListItem>
                    <asp:ListItem Value="2">18:00</asp:ListItem>
                    <asp:ListItem Value="3">20:00</asp:ListItem>
                    <asp:ListItem Value="4">22:00</asp:ListItem>
                </asp:DropDownList>
            </td>
            
        </tr>
       
        <tr>   
            <td>
                <asp:Label ID="lblFilme" runat="server" Text="Filme: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlFilme" runat="server" Width="170px" Height="30px"></asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblSala" runat="server" Text="Sala: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSala" runat="server" Width="170px" Height="30px"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Salvar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Limpar" OnClick="btnLimpar_Click"></asp:Button></td>
            <td></td>
        </tr>
        </table>
        
    </fieldset></center>

    <script src="../JS/pnotify.custom.min.js"></script>
    <script>
        function sucess() {
            new PNotify({
                title: 'Parabéns',
                text: 'Cadastrado com Sucesso!',
                type: 'success'
            });
        }
    </script>
    <script>
        function error() {
            new PNotify({
                title: 'Erro',
                text: 'Ocorreu um erro ao cadastrar!',
                type: 'error'
            });
        }
    </script>
</asp:Content>


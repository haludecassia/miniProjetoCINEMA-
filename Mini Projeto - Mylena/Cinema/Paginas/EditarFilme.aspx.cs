﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cinema.Funcoes;
using System.Data;

public partial class Paginas_EditarFilme : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if(Request.QueryString["fil"] != null && Request.QueryString["cat"] != null)
            {
                if(Request.QueryString["fil"] != "" && Request.QueryString["cat"] != "")
                {
                    try
                    {

                        string fil1 = Request.QueryString["fil"].ToString().Replace(" ", "+");
                        int n = Convert.ToInt32(Funcoes.AESDecodifica(fil1));
                        string cat1 = Request.QueryString["cat"].ToString().Replace(" ", "+");
                        int n1 = Convert.ToInt32(Funcoes.AESDecodifica(cat1));

                        Filme fil = FilmeDB.Select(n);

                        txbCodigo.Text = Convert.ToString(fil.fil_id);
                        txbNome.Text = fil.fil_nome;
                        txbSinopse.Text = fil.fil_sinopse;
                        txbDuracao.Text = fil.fil_duracao;
                        txbLancamento.Text = fil.fil_lancamento;

                        DataSet ds = CategoriaDB.SelectAll();
                        ddlCategoria.DataSource = ds;
                        ddlCategoria.DataTextField = "cat_descricao";
                        ddlCategoria.DataValueField = "cat_id";
                        ddlCategoria.DataBind();
                        ddlCategoria.Items.Insert(0, "Selecionar...");
                        ddlCategoria.SelectedIndex = n1;

                    }
                    catch (Exception erro)
                    {
                        Response.Redirect("~/Paginas/ConsultaFilme.aspx");
                    }


                }
            }
            else
            {
                Response.Redirect("~/Paginas/ConsultaFilme.aspx");
            }
        }
    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Filme fil = new Filme();
        Categoria cat = new Categoria();

        fil.fil_id = Convert.ToInt32(txbCodigo.Text);
        fil.fil_nome = txbNome.Text;
        fil.fil_sinopse = txbSinopse.Text;
        fil.fil_duracao = txbDuracao.Text;
        fil.fil_lancamento = txbLancamento.Text;
        fil.fil_categoria = cat;
        fil.fil_categoria.cat_id = Convert.ToInt32(ddlCategoria.SelectedValue);

        switch (FilmeDB.Atualizar(fil))
        {
            case 0:
                Response.Redirect("~/Paginas/ConsultaFilme.aspx");
                break;
            
        }

    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        switch (FilmeDB.Delete(Convert.ToInt32(txbCodigo.Text)))
        {
            case 0:
                Response.Redirect("~/Paginas/ConsultaFilme.aspx");
                break;
           
        }
        

    }
}
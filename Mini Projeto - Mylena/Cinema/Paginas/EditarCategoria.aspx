﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EditarCategoria.aspx.cs" Inherits="Paginas_EditarCategoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <center><fieldset>
        
        <legend>Editar Categorias</legend>
        

    <table>

        <tr>
            <td>
                <asp:Label ID="lblCodigo" runat="server" Text="Codigo: "></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txbCodigo" runat="server" CssClass="tb" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNome" runat="server" Text="Nome: "></asp:Label>
            </td>
            <td>    
                <asp:TextBox ID="txbNome" CssClass="tb" runat="server"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnSalvar" CssClass="Salvar" runat="server" Text="Gravar" OnClick="btnSalvar_Click"></asp:Button></td>
            <td><asp:Button ID="btnLimpar" CssClass="Limpar" runat="server" Text="Excluir" OnClick="btnLimpar_Click"></asp:Button></td>
            
        </tr>
        </table>
    </fieldset></center>


</asp:Content>

